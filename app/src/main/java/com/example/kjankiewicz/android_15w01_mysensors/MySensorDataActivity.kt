package com.example.kjankiewicz.android_15w01_mysensors

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_my_sensor_data.*

import java.text.DateFormat
import java.util.Calendar
import java.util.Locale
import java.util.TimeZone


class MySensorDataActivity : AppCompatActivity(), SensorEventListener {

    private var mSensorManager: SensorManager? = null
    private var mSensor: Sensor? = null

    private lateinit var valuesAdapter: IntArrayAdapter
    private lateinit var eventValues: Array<Float?>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_sensor_data)

        eventValues = arrayOfNulls(0)
        valuesAdapter = IntArrayAdapter(this, eventValues)

        eventSensorGridView.adapter = valuesAdapter

        val intent = intent
        val sensorType = intent.getIntExtra(MyMainActivity.SENSOR_TYPE, 0)

        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        if (sensorType > 0) {
            if (mSensorManager != null) {
                mSensor = mSensorManager!!.getDefaultSensor(sensorType)
            }

            mSensor?.apply {
                sNameTextView.text = String.format(getString(R.string.sensor_name), name)
                sTypeTextView.text = String.format(getString(R.string.sensor_type), type)
                sVendorTextView.text = String.format(getString(R.string.sensor_vendor), vendor)
                sVersionTextView.text = String.format(getString(R.string.sensor_version), version)
                sRangeTextView.text = String.format(getString(R.string.sensor_range), maximumRange)
                sMinDelayTextView.text = String.format(getString(R.string.sensor_delay), minDelay)
                sResolutionTextView.text = String.format(getString(R.string.sensor_resolution), resolution)
                sPowerTextView.text = String.format(getString(R.string.sensor_power), power)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mSensorManager!!.registerListener(this, mSensor, 1000000)
        // SensorManager.SENSOR_DELAY_NORMAL
    }

    override fun onPause() {
        super.onPause()
        mSensorManager!!.unregisterListener(this)
    }

    override fun onSensorChanged(event: SensorEvent) {
        sAccuracyTextView.text = String.format(getString(R.string.accuracy), Integer.toString(event.accuracy)) //"Accuracy: " + event.accuracy);

        val calendar = Calendar.getInstance()
        calendar.timeZone = TimeZone.getDefault()
        calendar.timeInMillis = System.currentTimeMillis() + (event.timestamp - System.nanoTime()) / 1000000L
        val dateFormat = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault()) //new SimpleDateFormat("HH:mm:ss");
        val dateString = dateFormat.format(calendar.time)
        sTimeTextView.text = String.format(getString(R.string.time), dateString)

        // check the leakage of resources
        eventValues = arrayOfNulls(event.values.size)
        for (i in event.values.indices) {
            eventValues[i] = event.values[i]
        }

        valuesAdapter = IntArrayAdapter(this, eventValues)
        eventSensorGridView.adapter = valuesAdapter

    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
        Toast.makeText(this, "Sensor Accuracy Changed to: $accuracy", Toast.LENGTH_LONG).show()
    }

    private inner class IntArrayAdapter internal constructor(c: Context, private val values: Array<Float?>) : ArrayAdapter<Float>(c, R.layout.event_item, values) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var rowView = convertView
            if (convertView == null) {
                val inflater = context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                rowView = inflater.inflate(R.layout.event_item, parent, false)
            }
            val eventValueTextView = rowView!!.findViewById<TextView>(R.id.eventValueTextView)
            val eventPositionTextView = rowView.findViewById<TextView>(R.id.eventPositionTextView)
            eventValueTextView.text = values[position].toString()
            eventPositionTextView.text = String.format(Locale.ENGLISH, "Values[%d]", position)
            return rowView
        }
    }
}

