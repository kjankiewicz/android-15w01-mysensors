package com.example.kjankiewicz.android_15w01_mysensors

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorManager
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_my_main.*


class MyMainActivity : AppCompatActivity() {

    private lateinit var deviceSensors: List<Sensor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_main)

        val mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL)

        val sensorsAdapter = SensorsAdapter(this, deviceSensors)

        sensorListView.adapter = sensorsAdapter

        sensorListView.setOnItemClickListener { parent, _, position, _ ->
            val sensor = parent.getItemAtPosition(position) as Sensor
            val intent = Intent(this@MyMainActivity, MySensorDataActivity::class.java)
            val sensorType = sensor.type
            intent.putExtra(SENSOR_TYPE, sensorType)
            startActivity(intent)
        }

        val sensorsType = arrayOf(Sensor.TYPE_ACCELEROMETER, Sensor.TYPE_PROXIMITY, Sensor.TYPE_MAGNETIC_FIELD, Sensor.TYPE_LIGHT, Sensor.TYPE_GRAVITY, Sensor.TYPE_AMBIENT_TEMPERATURE)

        val checkboxes = arrayOf(R.id.accelerometerCheckBox, R.id.proximityCheckBox, R.id.magneticCheckBox, R.id.lightCheckBox, R.id.gravityCheckBox, R.id.thermometerCheckBox)

        for (i in sensorsType.indices) {
            val cb = findViewById<CheckBox>(checkboxes[i])
            cb.isChecked = mSensorManager.getDefaultSensor(sensorsType[i]) != null
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.my_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        if (id == R.id.activity_my_location) {
            val intent = Intent(this@MyMainActivity, MyLocationActivity::class.java)
            startActivity(intent)
            return true
        } else if (id == R.id.get_last_known_location) {
            toastLastLocation()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun toastLastLocation() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSIONS_REQUEST_ACCESS_LOCATION)
        } else {
            val locationManager = this.getSystemService(
                    Context.LOCATION_SERVICE) as LocationManager
            val lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            Toast.makeText(this, "Last Known Location: \n " +
                    "Longitude/Długość/X :" +
                    Math.round(lastKnownLocation.longitude) + "\n" +
                    "Latitude/Szerokość/Y :" +
                    Math.round(lastKnownLocation.latitude), Toast.LENGTH_LONG).show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_ACCESS_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                toastLastLocation()
            } else {
                Toast.makeText(this,
                        "Until you grant the permission, your last known location cannot be displayed",
                        Toast.LENGTH_SHORT).show()
            }
        }
    }

    private inner class SensorsAdapter internal constructor(context: Context, sensors: List<Sensor>) : ArrayAdapter<Sensor>(context, R.layout.sensor_item, sensors) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val localConvertView: View =
                    convertView ?:
                    LayoutInflater.from(context).inflate(R.layout.sensor_item, parent, false)
            // Get the data item for this position
            val sensor = getItem(position)
            // Check if an existing view is being reused, otherwise inflate the view
            // Lookup view for data population
            val sensorNameTextView = localConvertView.findViewById<TextView>(R.id.sensorNameTextView)
            val sensorTypeTextView = localConvertView.findViewById<TextView>(R.id.sensorTypeTextView)
            val sensorRangeTextView = localConvertView.findViewById<TextView>(R.id.sensorRangeTextView)
            val sensorInfoTextView = localConvertView.findViewById<TextView>(R.id.sensorInfoTextView)
            // Populate the data into the template view using the data object
            if (sensor != null) {
                sensorNameTextView.text = String.format(getString(R.string.sensor_name), sensor.name)
                sensorTypeTextView.text = String.format(getString(R.string.sensor_type), sensor.type)
                sensorRangeTextView.text = String.format(getString(R.string.sensor_range), sensor.maximumRange)
                sensorInfoTextView.text = String.format(getString(R.string.sensor_info), sensor.vendor, sensor.power)
            }
            // Return the completed view to render on screen
            return localConvertView
        }
    }

    companion object {
        var SENSOR_TYPE = "SENSOR_TYPE"
        var PERMISSIONS_REQUEST_ACCESS_LOCATION = 1
    }
}
