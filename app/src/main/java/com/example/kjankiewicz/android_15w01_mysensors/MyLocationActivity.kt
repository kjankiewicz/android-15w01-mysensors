package com.example.kjankiewicz.android_15w01_mysensors

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_my_location.*


class MyLocationActivity : AppCompatActivity() {

    private lateinit var locationManager: LocationManager
    private lateinit var locationListener: LocationListener


    private var gpsProviderActiveBoolean: Boolean = false
    private var networkProviderActiveBoolean: Boolean = false

    internal var currentX = 0.0
    internal var currentY = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_location)

        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        val worldView = WorldView(this)
        worldView.layoutParams = layoutParams
        locationLinearLayout.addView(worldView)

        locationManager = this.getSystemService(
                Context.LOCATION_SERVICE) as LocationManager

        locationListener = object : LocationListener {

            override fun onLocationChanged(location: Location) {
                locLongitudeTextView.text = String.format(getString(R.string.value_X),
                        java.lang.Double.toString(location.longitude))
                locLatitudeTextView.text = String.format(getString(R.string.value_Y),
                        java.lang.Double.toString(location.latitude))
                locAltitudeTextView.text = String.format(getString(R.string.value_Z),
                        java.lang.Double.toString(location.altitude))
                locAccuracyTextView.text = String.format(getString(R.string.accuracy),
                        java.lang.Double.toString(location.accuracy.toDouble()))

                currentX = location.longitude
                currentY = location.latitude

                worldView.invalidate()
            }

            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
                Toast.makeText(this@MyLocationActivity, "Provider " + provider +
                        " status changed to " + status + ".", Toast.LENGTH_LONG).show()
            }

            override fun onProviderEnabled(provider: String) {
                Toast.makeText(this@MyLocationActivity, "Provider " + provider +
                        " enabled.", Toast.LENGTH_LONG).show()
            }

            override fun onProviderDisabled(provider: String) {
                locLongitudeTextView.text = String.format(getString(R.string.value_X), " --- ")
                locLatitudeTextView.text = String.format(getString(R.string.value_Y), " --- ")
                locAltitudeTextView.text = String.format(getString(R.string.value_Z), " --- ")
                locAccuracyTextView.text = String.format(getString(R.string.accuracy), " --- ")
                Toast.makeText(this@MyLocationActivity, "Provider " + provider +
                        " disabled.", Toast.LENGTH_LONG).show()
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.my_location, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        super.onPrepareOptionsMenu(menu)
        menu.findItem(R.id.gpsProviderActive).isChecked = gpsProviderActiveBoolean
        menu.findItem(R.id.networkProviderActive).isChecked = networkProviderActiveBoolean
        return true
    }

    /*    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }*/

    public override fun onResume() {
        super.onResume()
        registerForLocation()
    }

    private fun registerForLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSIONS_REQUEST_ACCESS_LOCATION)
        } else {
            gpsProviderActiveBoolean = false
            networkProviderActiveBoolean = false
            if (locationManager.allProviders.contains(
                            LocationManager.NETWORK_PROVIDER)) {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        1000, 5f, locationListener)
                networkProviderActiveBoolean = true
            }


            if (locationManager.allProviders.contains(
                            LocationManager.GPS_PROVIDER)) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        1000, 5f, locationListener)
                gpsProviderActiveBoolean = true
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_ACCESS_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                registerForLocation()
            } else {
                Toast.makeText(this,
                        "Until you grant the permission, your location cannot be displayed",
                        Toast.LENGTH_SHORT).show()
            }
        }
    }

    public override fun onPause() {
        locationManager.removeUpdates(locationListener)
        super.onPause()
    }

    private inner class WorldView(context: Context) : View(context) {

        internal var paint: Paint = Paint()
        internal var worldBitmap: Bitmap? = null

        init {
            paint.color = Color.RED
        }

        override fun onDraw(canvas: Canvas) {

            if (worldBitmap == null)
                worldBitmap = Bitmap.createScaledBitmap(
                        BitmapFactory.decodeResource(resources, R.drawable.world),
                        width, height, false)

            if (currentX == 0.0) return

            var x = width.toDouble() / 360
            x *= (currentX + 180)

            var y = height.toDouble() / 180
            y *= (90 - currentY)

            canvas.drawBitmap(worldBitmap!!, 0f, 0f, paint)

            canvas.drawCircle(x.toFloat(), y.toFloat(), 5f, paint)
            Log.i("Canvas Draw", "X:$x  Y:$y")
        }

        override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
            val width = measuredWidth
            setMeasuredDimension(width, width / 2)
        }
    }

    companion object {
        internal const val PERMISSIONS_REQUEST_ACCESS_LOCATION = 1
    }

}
